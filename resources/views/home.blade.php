
@extends('layout.welcome')
@section('content')
<div class="class-area text-center pt-80 pb-80">
    <div class="container">
        <div class="row g-4">
            <div class="col-lg-4 col-md-6">
                <div class="single-class picton-border">
                    <div class="class-icon">
                        <img src="{{ asset('assets/images/class/icon-1.webp') }}" alt="class">
                    </div>
                    <div class="class-content">
                        <h3>
                            <h3>Sauveteurs</h3></h3>
                        <h6>Instructor Adom Jonson </h6>
                        <p class="hover">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua. </p>
                        <div class="class-button">
                            <a href="#" class="read-more btn"><span>Voire plus</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-class cobalt-border">
                    <div class="class-icon">
                        <img src="{{ asset('assets/images/class/7.jpg') }}" alt="class">
                    </div>
                    <div class="class-content">
                        <h3>Croisière</h3>

                        <h6>Instructor Adom Jonson </h6>
                        <p class="hover">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua. </p>
                        <div class="class-button">
                            <a href="#" class="read-more btn"><span>Voire plus</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-class cinnabar-border">
                    <div class="class-icon">
                        <img src="{{ asset('assets/images/class/icon-3.webp') }}" alt="class">
                    </div>
                    <div class="class-content">
                        <h3>Victimes</h3>
                        <h6>Instructor Adom Jonson </h6>
                        <p class="hover">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua. </p>
                        <div class="class-button">
                            <a href="#" class="read-more btn"><span>Voire plus</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End of Class Area -->
<!-- Start of Appointment Area -->

<!-- End of Appointment Area -->
<!-- Start of Our Classes Area -->
<div class="our-classes-area pt-73 pb-30">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2>our classes</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseOne">
                                <span class="card-left"><img src="{{ asset('assets/images/icon/2.webp') }}" alt="icon">1 | 2-3 years</span>
                                <span class="card-middle">La SNSM</span>
                                <span class="card-right"><i class="zmdi zmdi-minus"></i><i class="zmdi zmdi-plus hover"></i></span>
                            </button>
                        </div>
                        <div id="collapseOne" class="collapse fade show" data-bs-parent="#accordionExample">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="class-left">


                                            <p class="heading-p">Class Time : 10am-11am</p>
                                            <p class="main">En France, chaque année la Société Nationale de Sauvetage en Mer, la SNSM, porte secours à 7700 personnes. Ses membres sont en veille toute l’année, entièrement dévoués à leur mission, quitte à prendre de risques pour sauver des vies.

                                                Depuis quelques années, alors que le trafic maritime s’intensifie, on constate une diminution des budgets alloués au sauvetage en mer.</p>
                                            <p class="heading-p">Totall Class - 20</p>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="class-right">
                                            <img src="{{ asset('assets/images/class/vv.jpg') }}" alt="class">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo">
                                <span class="card-left"><img src="{{ asset('assets/images/icon/2.webp') }}" alt="icon">2 | 3-4 years</span>
                                <span class="card-middle">Junior class</span>
                                <span class="card-right"><i class="zmdi zmdi-minus"></i><i class="zmdi zmdi-plus hover"></i></span>
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse fade" data-bs-parent="#accordionExample">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="class-left">
                                            <h3 class="login-title">swim clasess for junior</h3>
                                            <p class="heading-p">Age 3-4 Years</p>
                                            <p class="heading-p">Class Time : 11am-12am</p>
                                            <p class="main">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis notrud.</p>
                                            <p class="heading-p">Totall Class - 20</p>
                                            <button><span>Register Now</span></button>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="class-right">
                                            <img src="{{ asset('assets/images/icon/2.webp') }}" alt="class">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree">
                                <span class="card-left"><img src="{{ asset('assets/images/icon/2.webp') }}" alt="icon">3 | 4-5 years</span>
                                <span class="card-middle">Toddler class</span>
                                <span class="card-right"><i class="zmdi zmdi-minus"></i><i class="zmdi zmdi-plus hover"></i></span>
                            </button>
                        </div>
                        <div id="collapseThree" class="collapse fade" data-bs-parent="#accordionExample">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="class-left">
                                            <h3 class="login-title">swim clasess for infant</h3>
                                            <p class="heading-p">Age 4-5 Years</p>
                                            <p class="heading-p">Class Time : 10am-11am</p>
                                            <p class="main">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis notrud.</p>
                                            <p class="heading-p">Totall Class - 20</p>
                                            <button><span>Register Now</span></button>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="class-right">
                                            <img src="{{ asset('assets/images/class/3.webp') }}" alt="class">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="beginnerHeading">
                            <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#beginnerClass">
                                <span class="card-left"><img src="{{ asset('assets/images/class/3.webp') }}assets/images/icon/2.webp" alt="icon">4 | 5-6 years</span>
                                <span class="card-middle">Beginner class</span>
                                <span class="card-right"><i class="zmdi zmdi-minus"></i><i class="zmdi zmdi-plus hover"></i></span>
                            </button>
                        </div>
                        <div id="beginnerClass" class="collapse fade" data-bs-parent="#accordionExample">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="class-left">
                                            <h3 class="login-title">swim clasess for Beginner</h3>
                                            <p class="heading-p">Age 5-6 Years</p>
                                            <p class="heading-p">Class Time : 10am-11am</p>
                                            <p class="main">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis notrud.</p>
                                            <p class="heading-p">Totall Class - 20</p>
                                            <button><span>Register Now</span></button>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="class-right">
                                            <img src="{{ asset('assets/images/class/3.webp') }}assets/images/class/4.webp" alt="class">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="adultHeading">
                            <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#adultClass">
                                <span class="card-left"><img src="{{ asset('assets/images/class/3.webp') }}assets/images/icon/2.webp" alt="icon">5 | 6-12 years</span>
                                <span class="card-middle">Adult class</span>
                                <span class="card-right"><i class="zmdi zmdi-minus"></i><i class="zmdi zmdi-plus hover"></i></span>
                            </button>
                        </div>
                        <div id="adultClass" class="collapse fade" data-bs-parent="#accordionExample">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="class-left">
                                            <h3 class="login-title">swim clasess for adult</h3>
                                            <p class="heading-p">Age 6-12 Years</p>
                                            <p class="heading-p">Class Time : 10am-11am</p>
                                            <p class="main">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis notrud.</p>
                                            <p class="heading-p">Totall Class - 20</p>
                                            <button><span>Register Now</span></button>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="class-right">
                                            <img src="{{ asset('assets/images/class/3.webp') }}assets/images/class/5.webp" alt="class">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End of Our Classes Area -->
<!-- Start of Instructor Area -->
<div class="instructor-area bg-gray pt-76 pb-80">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2>our best instructor</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="slick-wrapper">
                    <div class="insturctor-owl">
                        <div class="custom-col">
                            <div class="single-instructor blue">
                                <div class="inst-img">
                                    <img src="{{ asset('assets/images/class/6.jpg') }}" alt="insturctor">
                                    <div class="inst-content text-center pt-36 pb-40 pl-30 pr-30">

                                        <p>les sauvetages? mais tout le monde en fait … si un homme tombe dans la « baille » est ce qu’on ne va pas le chercher? il n’y a à cela aucun mérite…</p>
                                        <ul>
                                            <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End of Instructor Area -->
<!-- Start of Testimonal Area -->
<div class="testimonial-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="testimonial-owl owl-carousel">
                    <div class="col">
                        <div class="testimonial-content text-center pt-75 pb-100 mt-80 mb-80">
                            <h3>Qui sont les sauveteurs de la SNSM ?</h3>
                            <i class="zmdi zmdi-quote first"></i>
                            <p>La SNSM compte 8 000 sauveteurs, tous bénévoles. Ce sont des marins aguerris, souvent marin-pêcheurs ou plaisanciers eux-mêmes.

                                Ils ont en moyenne 49 ans et s'engagent en moyenne 8 ans et 6 mois. On compte 12% de femmes sauveteurs.

                                Chaque membre de l’équipage a sa spécialité à bord : "patron", "radio", "mécanicien", "canotier", "secouriste", "nageur de bord" ou encore "plongeur de bord"</p>
                            <i class="zmdi zmdi-quote"></i>
                        </div>
                    </div>
                    <div class="col">
                        <div class="testimonial-content text-center pt-75 pb-100 mt-80 mb-80">
                            <h3>Combien de vies sont sauvées chaque année ?</h3>
                            <i class="zmdi zmdi-quote first"></i>
                            <p>Les sauveteurs appareillent sur demande des centres régionaux opérationnels de surveillance et de sauvetage (CROSS) en moins de 20 minutes, 24 heures sur 24, 365 jours par an et par tous les temps.

                                En 2018, les sauveteurs embarqués ont assuré près de 4 000 interventions de sauvetage et secouru 7 191 personnes.

                                Intervenant en tout temps, les sauveteurs prennent des risques importants car il arrive que des plaisanciers imprudents sortent sans réserve de carburant suffisant et/ou malgrè un avis de tempête. Ce fut le cas récemment au large de Saint-Vaast-la-Hougue.</p>
                            <i class="zmdi zmdi-quote"></i>
                        </div>
                    </div>
                    <div class="col">
                        <div class="testimonial-content text-center pt-75 pb-100 mt-80 mb-80">
                            <h3>Le modèle du sauvetage en mer doit-il être réformé ?</h3>
                            <i class="zmdi zmdi-quote first"></i>
                            <p>Le Premier Ministre Edouard Philippe a annoncé mardi 11 juin son souhait de réfléchir au "modèle du sauvetage en mer" qui repose sur le bénévolat et les dons.

                                "Nous devons nous interroger sur la permanence de ce modèle", a relevé Edouard Philippe, qui compte inscrire ce sujet à l'ordre du jour du prochain Comité interministériel de la mer, qu'il présidera à l'automne 2019.</p>
                            <i class="zmdi zmdi-quote"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End of Testimonal Area -->
<!--Start of gallery Area-->

<!-- End of gallery Area -->
<!-- Start of pricing Area -->

<!-- End of pricing Area -->
<!-- start of fun Area -->

<!-- End of fun Area -->
<!-- Start of blog Area -->
<div class="blog-area pt-80 pb-80">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12 mb-md--40">
                <div class="single-blog text-center">
                    <div class="blog-img">
                        <img src="{{ asset('assets/images/blog/8.jpg') }} " alt="blog">
                        <a href="#"><i class="zmdi zmdi-link"></i></a>
                        <div class="date">feb <span>21</span></div>
                    </div>
                    <div class="blog-content text-left">
                        <h3><a href="#">Swimming is The Best Physical Activity</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicinig elit, sed do eiusmod tempor incididunt ut labo rloreb magna alialiquip.</p>
                        <a href="#" class="read-more"><span>Voire plus</span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mb-md--40">
                <div class="single-blog text-center">
                    <div class="blog-img">
                        <img src="{{ asset('assets/images/blog/9.jpg') }}" alt="blog">
                        <a href="#"><i class="zmdi zmdi-link"></i></a>
                        <div class="date">feb <span>24</span></div>
                    </div>
                    <div class="blog-content text-left">
                        <h3><a href="#">Swimming is The Best Physical Activity</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicinig elit, sed do eiusmod tempor incididunt ut labo rloreb magna alialiquip.</p>
                        <a href="#" class="read-more"><span>Voire plus</span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <div class="single-blog text-center">
                    <div class="blog-img">
                        <img src="{{ asset('assets/images/blog/10.jpg') }}" alt="blog">
                        <a href="#"><i class="zmdi zmdi-link"></i></a>
                        <div class="date">feb <span>26</span></div>
                    </div>
                    <div class="blog-content text-left">
                        <h3><a href="#">Thomas Leigh Gatch Jr.</a></h3>
                        <p>American balloonist Thomas Gatch disappeared while attempting to become the first human to cross the Atlantic by balloon. A day after lifting off from Harrisburg Airport on 18 February, his balloon named Light Heart lost radio contact. On 21 February, it was sighted by a ship about 1600 km west of the Canary Islands but it has not been seen since.
                        </p>
                        <a href="#" class="read-more"><span>Voire plus</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End of classes Area -->
<!-- Start of contact Area -->
<div class="contact-area text-center pb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="single-contact">
                    <div class="contact-icon">
                        <i class="zmdi zmdi-home"></i>
                    </div>
                    <div class="contact-info">
                        <h5>Address</h5>
                        <p>Elizabeth Tower, 6th Floor<br> Medtown, New York</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="single-contact">
                    <div class="contact-icon">
                        <i class="zmdi zmdi-phone"></i>
                    </div>
                    <div class="contact-info">
                        <h5>phone number</h5>
                        <p>0123456789<br>
                            0123456789</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="single-contact">
                    <div class="contact-icon">
                        <i class="zmdi zmdi-email"></i>
                    </div>
                    <div class="contact-info">
                        <h5>email</h5>
                        <p>demo@example.com<br>
                            demo@example.com </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End of contact Area -->
<!-- Start of Footer area -->
<footer class="footer-area">
    <div class="main-footer pt-76 pb-72">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-7 col-12 mb-md--40">
                    <div class="footer-widget text-left f-left">
                        <h3>twitter Feed</h3>
                        <div class="single-feed mb-15">
                            <h5>Post By: John Doe <span>12 Feb 2017</span></h5>
                            <p><a href="#">#There</a> are may <a href="#">“varations”</a> pasages of texi Lorem Ipsum available njhgh fhg excercise</p>
                        </div>
                        <div class="single-feed">
                            <h5>Post By: John Doe <span>12 Feb 2017</span></h5>
                            <p>There are may <a href="#">#“variations”</a> pasages of texi</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-12 mb-md--40">
                    <div class="footer-widget text-center">
                        <a href="index.html"><img src="assets/images/logo/footer-logo.webp" alt="logo"></a>
                        <p>Lorem ipsum dolor sit amet, consectetur adip isicing elit, seo eiusmod tempor in hjgcididunt ut labore eagna al veniam, quis nostrud </p>
                        <div class="footer-icons">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-12">
                    <div class="footer-widget f-right">
                        <h3 class="text-left">latest post</h3>
                        <div class="post">
                            <div class="post-img">
                                <img src="{{ asset('assets/images/class/3.webp') }}assets/images/post/1.webp" alt="post">
                                <a href="#">
                                    <i class="zmdi zmdi-link"></i>
                                </a>
                            </div>
                            <div class="post-img">
                                <img src="{{ asset('assets/images/class/3.webp') }}assets/images/post/2.webp" alt="post">
                                <a href="#">
                                    <i class="zmdi zmdi-link"></i>
                                </a>
                            </div>
                            <div class="post-img">
                                <img src="{{ asset('assets/images/class/3.webp') }}assets/images/post/3.webp" alt="post">
                                <a href="#">
                                    <i class="zmdi zmdi-link"></i>
                                </a>
                            </div>
                            <div class="post-img">
                                <img src="{{ asset('assets/images/class/3.webp') }}assets/images/post/4.webp" alt="post">
                                <a href="#">
                                    <i class="zmdi zmdi-link"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom pt-8 pb-10">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <p class="copyright">&copy; 2021 <strong>Swimmer</strong> Made with <i class="fa fa-heart text-danger" aria-hidden="true"></i> by <a href="https://hasthemes.com/"><strong>HasThemes</strong></a>.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End of Footer area -->
@endsection
